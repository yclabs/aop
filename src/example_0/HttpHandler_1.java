package example_0;

public class HttpHandler_1 {

	
	public void doGet() {
		long before = System.nanoTime();
		
		try {
			System.out.println("GET method execution...");
			Thread.sleep((long)(Math.random() * 1000));
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		long after = System.nanoTime();
		System.err.println("GET_EXECUTION_TIME: " + (after-before));
	}
	
	
	public void doPost() {
		long before = System.nanoTime();
		
		try {
			System.out.println("POST method execution...");
			Thread.sleep((long)(Math.random() * 1000));
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		long after = System.nanoTime();
		System.err.println("POST_EXECUTION_TIME: " + (after-before));
	}
}
