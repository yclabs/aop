package example_0;

public aspect ExecutionMeasurementAspect {

	pointcut measureExecution() : execution(* example_0.HttpHandler_2.*(..));
	
	void around() : measureExecution() {
		long before = System.nanoTime();
		
		proceed();
		
		long after = System.nanoTime();
		
		String methodName = thisJoinPoint.getSignature().getName();
		System.err.println(methodName + "_EXECUTION_TIME: " + (after-before));
	}
}
