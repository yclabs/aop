package example_0;

public class HttpHandler_2 {

	public void doGet() {
		try {
			System.out.println("GET method execution...");
			Thread.sleep((long)(Math.random() * 1000));
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public void doPost() {
		try {
			System.out.println("POST method execution...");
			Thread.sleep((long)(Math.random() * 1000));
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
