package example_3;

public class Test {

	public static void main(String[] args) {
		
		Student student = new Student("Uros");
		new Student("Pera");
		new Student("Laza");
		
		student.setIndeks(-5);
		
//		System.out.println(student);
		
		
		// student
		// biznis logika -> ne-negativan indeks
		
		// pokusaj-1:
		// provera u setteru i poziv settera u konstruktoru
		
		// pokusaj-2:
		// izvuci proveru van klase student, jer je podlozna promeni
		// utility klasa? => lose za odrzavanje, nije reusabilno, neko mora da se poziva na utility klasu
		
		
		// resenje
		
		// aspect: Inter-Type -> assertIndex
		
		// student.assertIndex
		
		// aspect: Inter-Type -> static nextIndex
		
		// spreciti poziv setIndeks ukoliko se izvrsava indexGenerator
		
		// zakljucak: clean Student
	}	
}
