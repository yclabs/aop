package example_3;

public class Student {

	private int indeks;
	private String ime;
	
	public Student() {
		this("");
	}
	
	public Student(String ime) {
		this(0, ime);
	}
	
	public Student(int indeks, String ime) {
		setIndeks(indeks);
		this.ime = ime;
	}

	public int getIndeks() {
		return indeks;
	}

	public void setIndeks(int indeks) {
//		if (indeks < 0)
//			throw new IllegalArgumentException("Indeks ne moze biti negativna vrednost!");
		this.indeks = indeks;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	@Override
	public String toString() {
		return "Student [indeks=" + indeks + ", ime=" + ime + "]";
	}
}
