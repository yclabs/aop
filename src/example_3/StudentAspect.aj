package example_3;

public aspect StudentAspect {

	private static int Student.nextIndex;
	
	boolean Student.assertIndex(int index) {
		return index >= 0;
	}
	
	pointcut indexGenerator(Student s) : this(s) && initialization(public Student.new(..));
	
	pointcut indexSetter(Student s, int indeks) : target(s) && args(indeks) && call(public void Student.setIndeks(int)) && !cflow(indexGenerator(Student));
	
	before(Student s, int indeks) : indexSetter(s, indeks) {
		System.out.println("setIndeks invoked");
		if ( !s.assertIndex(indeks) ) {
			throw new IllegalArgumentException("Indeks ne moze biti negativna vrednost - VALUE: " + indeks);
		}
	}
	
	after(Student s) : indexGenerator(s) {
		s.setIndeks(Student.nextIndex++);
		System.out.println(s);
	}
}
