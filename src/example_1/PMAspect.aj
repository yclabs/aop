package example_1;

import java.io.PrintStream;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.reflect.SourceLocation;

import example_1.persistence.PersistenceManager;
import example_1.persistence.Transaction;
import example_1.persistence.Transactional;

@SuppressWarnings("rawtypes")
public aspect PMAspect {

	private PrintStream logStream = System.out;
	private PrintStream errStream = System.err;
	
	
	pointcut log() : execution(public !static * persistence.PersistenceManager.*(..)) && !execution(* persistence.PersistenceManager.getUser());
	
	after(): log() {
		SourceLocation location = thisJoinPoint.getSourceLocation();
		logStream.println("LOG: " + location.getFileName() + " at: " + location.getLine());
	}
	
	
	pointcut secured() : execution(public !static * *.PersistenceManager.*(..)) && (@annotation(Secured));
	
	void around() : secured() {
		PersistenceManager<?> pm = (PersistenceManager)thisJoinPoint.getThis();
		
		User user = pm.getUser();
		
		if (user == null || !user.isLogged()) {
			errStream.println("The user " + user.getUsername() + " is not logged in!");
			return;
		}
		
		proceed();
	}
	
	
	
	pointcut transactional() : execution(public !static void *.PersistenceManager.*(..)) && (@annotation(Transactional));
	
	void around() : transactional() {
		PersistenceManager<?> pm = (PersistenceManager)thisJoinPoint.getThis();
		
		Transaction tr = pm.beginTransaction();
		try {
			proceed();
			
			tr.commit();
		}
		catch(Exception e) {
			tr.rollback();
		}
		finally {
			pm.close();
		}
	}
}
