package example_1;

public class User {

	private boolean logged;
	private String username;
	
	public User(String username) {
		this.username = username;
	}
	
	public User() {
		this("Guest");
	}
	
	public User(User user) {
		this.username = user.username;
		this.logged = user.logged;
	}
	
	public boolean isLogged() {
		return logged;
	}
	
	public void setLogged(boolean logged) {
		this.logged = logged;
	}
	
	public String getUsername() {
		return username;
	}

	@Override
	public String toString() {
		return "User [username=" + username + ", logged=" + logged + "]";
	}
}
