package example_1;

import example_1.persistence.PersistenceManager;

public class Test {

	public static void main(String[] args) {
		
		PersistenceManager<Student> pm = PersistenceManager.getInstance();
		
		Student st1 = new Student(27, "Uros", "Krkic");
		
		pm.persist(st1);
		
		pm.delete(st1);
		
		Student st2 = pm.findByPrimaryKey(Student.class, Integer.valueOf(27));
		
		System.out.println(st2);
		
		System.out.println("================================================");
		
		User user = new User("admin");
		user.setLogged(true);
		PersistenceManager<Student> pm2 = PersistenceManager.createInstance(user);
		pm2.persist(st1);
		
		System.out.println("================================================");
		
		pm2.delete(st1);
	}
	
}
