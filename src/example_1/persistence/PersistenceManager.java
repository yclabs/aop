package example_1.persistence;

import example_1.Secured;
import example_1.Student;
import example_1.User;

@SuppressWarnings({"rawtypes", "unchecked"})
public class PersistenceManager<T> {

	private static PersistenceManager instanceHolder = new PersistenceManager();
	
	private User user;
	
	
	public static <T> PersistenceManager<T> getInstance() {
		return instanceHolder;
	}
	
	public static <T> PersistenceManager<T> createInstance(User user) {
		return new PersistenceManager<T>(user);
	}
	
	
	private PersistenceManager() {
		this(new User());
	}
	
	private PersistenceManager(User user) {
		this.user = user;
	}
	
	public User getUser() {
		return new User(user);
	}
	
	
	public Transaction beginTransaction() {
		System.out.println("Transaction BEGIN");
		return new Transaction();
	}
	
	public void close() {
		System.out.println("PersistenceManager closed.");
	}
	
	
	
	@Secured
	public void persist(T object) {
		if (!object.getClass().isAnnotationPresent(Persistable.class)) {
			throw new IllegalArgumentException("Object is not persistable");
		}
		System.out.println("--> EM_CALL: Persist object: " + object);
	}
	
	
	@Secured
	@Transactional
	public void delete(T object) {
		System.out.println("--> EM_CALL: Delete object: " + object);
	}
	
	
	public T findByPrimaryKey(Class<T> clazz, Object primaryKey) {
		System.out.println("--> EM_CALL: findByPrimaryKey: " + primaryKey);
		
		try {
//			return clazz.newInstance();
			
			// fake
			return (T) new Student(27, "Uros", "Krkic");
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
}
