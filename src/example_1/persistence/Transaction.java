package example_1.persistence;

public class Transaction {
	
	public void commit() {
		System.out.println("Transaction COMMIT");
	}
	
	public void rollback() {
		System.out.println("Transaction ROLLBACK");
	}
}
