package example_2;

public class TestInjection {

	public static void main(String[] args) {
		
		ServicesSessionBean sessionBean = new ServicesSessionBean();
		
		sessionBean.saveOrder("Order-1");
		sessionBean.saveOrder("Order-2");
		sessionBean.deleteOrder("Order-3");
	}
	
}
