package example_2;

public class ServicesSessionBean {

	@DependencyInjection
	private EntityManager pm;
	
	
	public void saveOrder(String order) {
		pm.persist(order);
	}
	
	
	public void deleteOrder(String order) {
		pm.remove(order);
	}
}
