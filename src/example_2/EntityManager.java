package example_2;

public class EntityManager {

	public void persist(String object) {
		System.out.println("Persisting: " + object);
	}
	
	public void remove(String object) {
		System.out.println("Removing: " + object);
	}
	
}
