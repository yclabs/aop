package example_2;

import java.lang.reflect.Field;

public aspect DependencyInjectionAspect {

//	pointcut injector() : initialization(public example_2.*.new(..));
//	pointcut injector() : initialization(public *.new(..));
	
	pointcut injector() : initialization(public example_2.*.new(..)) && !within(DependencyInjectionAspect);
	
	after() : injector() {
		Field[] fields = thisJoinPoint.getThis().getClass().getDeclaredFields();
	
		for (Field field : fields) {
			if (field.isAnnotationPresent(DependencyInjection.class)) {
				injectField(thisJoinPoint.getThis(), field);
			}
		}
	}
	
	
	void injectField(Object obj, Field field) {
		try {
			Object value = field.getType().newInstance();
			field.setAccessible(true);
			field.set(obj, value);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	

	
	// #######################################################
	
	pointcut thisVsTarget() : call(public * example_2.*.*(..)) && !within(TestInjection);
	
	after() : thisVsTarget() {
		System.out.println("THIS: " + thisJoinPoint.getThis());
		System.out.println("TARGET: " + thisJoinPoint.getTarget());
		System.out.println("=======================================================");
	}
	
}
